angular.module('myApp.templates',['ngRoute', 'angularUtils.directives.dirPagination'])

.config(['$routeProvider', function($routeProvider) {
	$routeProvider.
		when('/templates', {
			templateUrl: 'templates/templates.html',
			controller: 'TemplatesCtrl'
		}).
		when('/templates/:templateId', {
			templateUrl: 'templates/template-details.html',
			controller: 'TemplateDetailsCtrl'
		}).
		otherwise({redirectTo: '/templates'});


}])

.config(function(paginationTemplateProvider) {
	paginationTemplateProvider.setPath('/templates/dirPagination.tpl.html');
})

.controller('TemplatesCtrl', ['$scope', '$http', function($scope, $http){
	
	$http.get('json/cars.json').success(function(data){
		$scope.templates = data.cars;
	})
}])

.controller('TemplateDetailsCtrl', ['$scope', '$routeParams', '$http', '$filter', function($scope, $routeParams, $http, $filter){
	var templateId = $routeParams.templateId;
	$http.get('json/cars.json').success(function(data){
		$scope.template = $filter('filter')(data.cars, function(d){
			return d.id == templateId;
		})[0];
		$scope.main_picture = $scope.template.main_picture;
	})
}]).run(['$location', function ($location) {
	$location.path('/templates');
}])